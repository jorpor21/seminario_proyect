<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Instrucciones</title>
    <script type="text/javascript" src="Librerias/Librerias.js"></script>

    <!-- Bootstrap -->
    
      <link rel="stylesheet" href="css/bootstrap.min2.css">
      <link rel="stylesheet" href="css/navbar-static-top.css">
      
      <script type="text/javascript" src="js/script.js"></script>
      <script src="js/jquery.js"></script>
      <script src="js/main.js"></script>
      <script>verificarseg2();   </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>

       <!-- Encabezado -->
    <div class="active item">
          <img src="sistema.png" class="img-thumbnail" />
      </div>





      
   <!--menu-->
    
<div class="container">
  <div class="row clearfix">
    <div class="col-md-12 column">

 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Bienvenido</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;   Inicio &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  <span class="sr-only">(current)</span></a></li>
        <li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  Nosotros &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  </a></li>
        <li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  Contacto &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;</a></li>
      </ul>
      <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Busqueda">
        </div>
        <button type="submit" class="btn btn-default">Buscar</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
      </ul>
    </div>
  </div>
</nav>
    
    </div>
  </div>
</div>





<div class="container">
  <div class="row clearfix">
    <div class="col-md-12 column">
      <div class="carousel slide" id="carousel-562148">
        <ol class="carousel-indicators">
          <li class="active" data-slide-to="0" data-target="#carousel-562148">
          </li>
          <li data-slide-to="1" data-target="#carousel-562148">
          </li>
          <li data-slide-to="2" data-target="#carousel-562148">
          </li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <img alt="" src="slider1.jpg" />
            
            <div class="carousel-caption">
              <h4>
                Sistema de Administración de ordenes
              </h4>
              <p>
                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
              </p>
            </div>
          </div>
          <div class="item">
           <img alt="" src="slider2.jpg" />
            <div class="carousel-caption">
              <h4>
                Second Thumbnail label
              </h4>
              <p>
                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
              </p>
            </div>
          </div>
          <div class="item">
            <img alt="" src="slider3.jpg" />
            <div class="carousel-caption">
              <h4>
                Third Thumbnail label
              </h4>
              <p>
                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
              </p>
            </div>
          </div>
        </div> <a class="left carousel-control" href="#carousel-562148" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-562148" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
      </div>
    </div>
  </div>
</div>
       <!-- cuerpo-->


<br/>

<div class="container">
  <div class="row clearfix">
    <div class="col-md-6 column">

     <div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title">Alcaldía de Cordoba</h2>
  </div>
  <div class="panel-body">
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui
  </div>

</div>

    </div>
    <div class="col-md-6 column">

<div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title">Iniciar Sesión</h2>
  </div>
  <div class="panel-body">
    
    <form class="form-horizontal"  id="auth"  role= "form">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Usuario</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="usuario" placeholder="Ingrese Usuario o Email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" id="password" placeholder="Ingrese Contraseña">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="button" class="btn btn-default"  id="login" onclick='autenticar();' >Iniciar</button>
    </div>
  </div>
</form>
  </div>
</div>

    </div>
  </div>
  <div class="row clearfix">
    <div class="col-md-12 column">




    </div>
  </div>
</div>

      <div class="active item">
          <img src="sistema2.png" class="img-thumbnail" />
      </div>

   <script src="js/bootstrap.min2.js"></script>
      <script src="js/main.js"></script>
    </body>



</html>