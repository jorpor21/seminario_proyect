-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-04-2015 a las 17:08:41
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sao`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE IF NOT EXISTS `cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `nombre`, `tipo`) VALUES
(1, 'Jefe', 0),
(2, 'Secretario/a', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DPO_id` int(11) DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `DPO_id` (`DPO_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id`, `DPO_id`, `nombre`) VALUES
(1, NULL, 'Alcaldia'),
(2, 1, 'Coodinación General'),
(3, 2, 'Recursos Humanos'),
(5, 1, 'Institutos Autonomos'),
(6, 1, 'Otros'),
(7, 2, 'Hacienda'),
(8, 2, 'Ingenieria'),
(9, 8, 'Ingeniería Municipal'),
(10, 8, 'Vialida Agricola'),
(11, 8, 'Obras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE IF NOT EXISTS `estatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`id`, `nombre`, `activo`) VALUES
(1, 'Pendiente', 1),
(2, 'En Ejecución', 1),
(3, 'Completado', 1),
(4, 'Cancelado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etapa`
--

CREATE TABLE IF NOT EXISTS `etapa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ITO_id` int(11) NOT NULL,
  `ETU_id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ITO_id` (`ITO_id`,`ETU_id`),
  KEY `ETU_id` (`ETU_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `etapa`
--

INSERT INTO `etapa` (`id`, `ITO_id`, `ETU_id`, `fecha`) VALUES
(1, 1, 1, '2014-12-03 19:41:34'),
(2, 2, 2, '2014-12-03 19:41:34'),
(3, 1, 2, '2014-12-03 19:55:25'),
(4, 1, 3, '2014-12-03 20:12:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instruccion`
--

CREATE TABLE IF NOT EXISTS `instruccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(200) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_entrega` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Volcado de datos para la tabla `instruccion`
--

INSERT INTO `instruccion` (`id`, `titulo`, `descripcion`, `fecha_creacion`, `fecha_entrega`) VALUES
(1, 'prueba', 'debe contestar todos los requerimientos', '2014-12-03 17:11:45', '2014-12-05 12:30:00'),
(2, 'prueba 2', 'debe contestar todos los requerimientos', '2014-12-03 17:11:45', '2014-12-06 09:53:00'),
(3, 'hola mundo', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:03:47', '2014-12-26 04:30:00'),
(4, 'hola mundo', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:07:58', '2014-12-26 04:30:00'),
(5, 'hola mundo', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:08:47', '2014-12-26 04:30:00'),
(6, 'hola mundo', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:09:54', '2014-12-26 04:30:00'),
(7, 'hola mundo', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:11:26', '2014-12-26 04:30:00'),
(8, 'hola mundo2', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:39:54', '2014-12-26 04:30:00'),
(9, 'hola mundo2', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:42:48', '2014-12-26 04:30:00'),
(10, 'hola mundo3', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:47:30', '2014-12-26 04:30:00'),
(11, 'hola mundo3', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:50:36', '2014-12-26 04:30:00'),
(12, 'hola mundo3', 'este es el end}sayo de insercion por formulario', '2014-12-04 00:53:33', '2014-12-26 04:30:00'),
(13, 'hola mundo3', 'este es el end}sayo de insercion por formulario', '2014-12-04 01:06:37', '2014-12-26 04:30:00'),
(14, 'hola mundo3', 'este es el end}sayo de insercion por formulario', '2014-12-04 01:12:42', '2014-12-26 04:30:00'),
(15, 'hola mundo3', 'este es el end}sayo de insercion por formulario', '2014-12-04 01:14:08', '2014-12-26 04:30:00'),
(16, 'hola mundo3', 'este es el end}sayo de insercion por formulario', '2014-12-04 01:14:42', '2014-12-26 04:30:00'),
(17, 'hola mundo4', 'este es el end}sayo de insercion por formulario', '2014-12-04 01:20:56', '2014-12-26 04:30:00'),
(18, 'hola mundo4', 'este es el end}sayo de insercion por formulario', '2014-12-04 01:24:13', '2014-12-26 04:30:00'),
(19, 'hola mundo4', 'este es el end}sayo de insercion por formulario', '2014-12-04 01:26:44', '2014-12-26 04:30:00'),
(20, 'jorman', 'szsxcfvgbhnjmk,lmknjvgfdx hjknuiyuv bvyfc gvhg ', '2014-12-04 03:51:00', '2014-12-27 04:30:00'),
(21, 'la mejor', 'toda bien ', '2014-12-04 16:53:19', '2014-12-20 04:30:00'),
(22, 's', ' s', '2014-12-04 17:42:58', '2014-12-12 04:30:00'),
(23, 'MITITULO', 'MIDESCRIPCION ', '2014-12-04 19:00:52', '2014-12-18 04:30:00'),
(24, 'primera api', 'esta es la primera insercion', '2015-03-22 20:14:00', '2015-04-10 04:30:00'),
(25, 'segunda instruccion', 'esta es la segunda instrucción ', '2015-03-22 20:21:28', '2015-03-05 04:30:00'),
(26, 'portela', 'savsdf sgh s h s', '2015-03-22 20:32:47', '2015-03-20 04:30:00'),
(27, 'trecer orden', 'esta es la tercer orden', '2015-03-22 20:47:01', '2015-01-01 04:30:00'),
(28, 'cuarta orden', 'esta es la cuarta orden', '2015-03-22 20:51:48', '2015-01-01 04:30:00'),
(29, 'quinta orden', 'esta es la quinta orden', '2015-03-22 20:57:50', '2015-01-01 04:30:00'),
(30, 'SESTA orden', 'esta es la SESTA orden', '2015-03-22 21:04:24', '2015-01-01 04:30:00'),
(31, 'septima ', 'esta es la septima', '2015-03-22 21:10:33', '2015-01-01 04:30:00'),
(32, 'octava', 'esta es la octava', '2015-03-22 21:21:19', '2015-01-01 04:30:00'),
(33, 'novena', 'esta es la novena', '2015-03-22 22:48:43', '2015-03-12 04:30:00'),
(34, 'Presentar Proyecto Seminario', 'Aún así, existirán casos en que queramos que un texto contenga simultáneamente comillas simples y dobles, con lo que la solución anterior no nos resulta satisfactoria. Para resolver estas situaciones, se usa el denominado carácter backslah o carácter de escape, que es el símbolo \\.', '2015-03-24 20:17:00', '2015-03-13 04:30:00'),
(35, 'revision', 'revio para alizar', '2015-03-24 21:48:22', '2015-01-23 04:30:00'),
(36, 'revision useche', 'thfvjhv  fgjbhgh k ', '2015-03-24 22:03:32', '2015-01-23 04:30:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE IF NOT EXISTS `modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `modulo`
--

INSERT INTO `modulo` (`id`, `nombre`, `estatus`) VALUES
(1, 'usuarios', 1),
(2, 'departamentos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion`
--

CREATE TABLE IF NOT EXISTS `notificacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `USO_RPE_id` int(11) NOT NULL,
  `ITO_RPE_id` int(11) NOT NULL,
  `notificacion` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `USO_RPE_id` (`USO_RPE_id`,`ITO_RPE_id`),
  KEY `ITO_RPE_id` (`ITO_RPE_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupacion`
--

CREATE TABLE IF NOT EXISTS `ocupacion` (
  `DPO_id` int(11) NOT NULL,
  `CGO_id` int(11) NOT NULL,
  `USO_id` int(11) NOT NULL,
  PRIMARY KEY (`DPO_id`,`USO_id`),
  KEY `CGO_id` (`CGO_id`),
  KEY `USO_id` (`USO_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ocupacion`
--

INSERT INTO `ocupacion` (`DPO_id`, `CGO_id`, `USO_id`) VALUES
(2, 1, 1),
(3, 1, 4),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `ROL_id` int(11) NOT NULL,
  `MDO_id` int(11) NOT NULL,
  `insertar` tinyint(1) NOT NULL DEFAULT '1',
  `modificar` tinyint(1) NOT NULL DEFAULT '1',
  `eliminar` tinyint(1) NOT NULL DEFAULT '1',
  `consultar` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ROL_id`,`MDO_id`),
  KEY `ROL_id` (`ROL_id`),
  KEY `MDO_id` (`MDO_id`),
  KEY `ROL_id_2` (`ROL_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`ROL_id`, `MDO_id`, `insertar`, `modificar`, `eliminar`, `consultar`) VALUES
(1, 1, 1, 1, 1, 1),
(1, 2, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsable`
--

CREATE TABLE IF NOT EXISTS `responsable` (
  `USO_id` int(11) NOT NULL,
  `ITO_id` int(11) NOT NULL,
  `creador` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`USO_id`,`ITO_id`),
  KEY `ITO_id` (`ITO_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `responsable`
--

INSERT INTO `responsable` (`USO_id`, `ITO_id`, `creador`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 18, 0),
(1, 19, 0),
(1, 20, 0),
(1, 21, 0),
(1, 22, 0),
(1, 23, 0),
(1, 25, 0),
(1, 26, 0),
(1, 30, 0),
(1, 31, 0),
(1, 32, 0),
(1, 33, 0),
(1, 35, 0),
(1, 36, 0),
(2, 1, 0),
(2, 2, 0),
(2, 19, 0),
(2, 20, 0),
(2, 21, 0),
(2, 22, 0),
(2, 23, 0),
(2, 25, 0),
(2, 26, 0),
(2, 30, 0),
(2, 31, 0),
(2, 32, 0),
(2, 34, 0),
(2, 35, 0),
(2, 36, 0),
(3, 20, 0),
(4, 1, 0),
(4, 19, 0),
(4, 20, 0),
(4, 24, 0),
(4, 27, 0),
(4, 28, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `estatus`) VALUES
(1, 'Administrador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `primer_nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `segundo_nombre` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `primer_apellido` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `segundo_apellido` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` bigint(11) DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `login` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `contraseña` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `recibe_instruc` tinyint(1) NOT NULL DEFAULT '1',
  `ROL_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula` (`cedula`,`email`,`login`),
  KEY `ROL_id` (`ROL_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `cedula`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `telefono`, `email`, `login`, `contraseña`, `recibe_instruc`, `ROL_id`) VALUES
(1, 21001887, 'Johan', 'Manuel', 'Castillo', 'Fernandez', 4148873248, 'johanmcastillof@gmail.com', 'jcastillo', '123456', 0, 1),
(2, 21418178, 'Jorman', 'David', 'Portela', 'Benitez', 4149776916, 'jormand91@gmail.com', 'jportela', '123456', 1, 1),
(3, 2112412341, 'juan', NULL, 'Perez', NULL, 1234567, 'alex_frerreira@hotmail.com', 'jhni', 'sfsdgd', 0, 1),
(4, 20617266, 'maria', NULL, 'duran', NULL, 4147220718, 'duranrojasm@gmail.com', 'mariad', '123456', 0, 1),
(5, 2222, 'hgggtfffdtfd', NULL, 'uvjgufu', NULL, 6757565, 'dfghj@dfgghj.co', 'ffff', '1234567', 0, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`DPO_id`) REFERENCES `departamento` (`id`);

--
-- Filtros para la tabla `etapa`
--
ALTER TABLE `etapa`
  ADD CONSTRAINT `etapa_ibfk_1` FOREIGN KEY (`ITO_id`) REFERENCES `instruccion` (`id`),
  ADD CONSTRAINT `etapa_ibfk_2` FOREIGN KEY (`ETU_id`) REFERENCES `estatus` (`id`);

--
-- Filtros para la tabla `notificacion`
--
ALTER TABLE `notificacion`
  ADD CONSTRAINT `notificacion_ibfk_1` FOREIGN KEY (`USO_RPE_id`) REFERENCES `responsable` (`USO_id`),
  ADD CONSTRAINT `notificacion_ibfk_2` FOREIGN KEY (`ITO_RPE_id`) REFERENCES `responsable` (`ITO_id`);

--
-- Filtros para la tabla `ocupacion`
--
ALTER TABLE `ocupacion`
  ADD CONSTRAINT `ocupacion_ibfk_1` FOREIGN KEY (`DPO_id`) REFERENCES `departamento` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ocupacion_ibfk_2` FOREIGN KEY (`CGO_id`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `ocupacion_ibfk_3` FOREIGN KEY (`USO_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`ROL_id`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`MDO_id`) REFERENCES `modulo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `responsable`
--
ALTER TABLE `responsable`
  ADD CONSTRAINT `responsable_ibfk_3` FOREIGN KEY (`USO_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `responsable_ibfk_4` FOREIGN KEY (`ITO_id`) REFERENCES `instruccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`ROL_id`) REFERENCES `rol` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
