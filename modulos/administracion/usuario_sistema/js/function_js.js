/***********************************************************************************
							FUNCION AJAX PARA EJECUCION DE USUARIO								
***********************************************************************************/
var url_ubicacion = "";
var url= "";

function ejecutarUsuario(parametros)
{	
  	$.ajax({ async:true, type: "GET", dataType: "html", url:"modulos/administracion/usuario_sistema/ajax/ejecutarUsuariosis.php", data:parametros, beforeSend:inicioUsuario, success:llegadaDatosUsuario, timeout:6000, error:problemasUsuario }); 
	return false;
}

function inicioUsuario()
{

      $("#ejecutarUsuario").html("<IMG src='" + url_ubicacion + "img/cargando.gif' height='15' width='15' alt='Cargando...'>");
}

function llegadaDatosUsuario(datos)
{
	setTimeout("javascript:updateView('modulos/administracion/usuario_sistema/interfaz.php?v=4&id="+ datos +"')"); 
}

function problemasUsuario()
{
	$("#ejecutarUsuario").text('Problemas en el Servidor.');
}

/*************************************************************************************
      FUNCION PARA VALIDAR EL MODULO DE CAMBIO DE CLAVE
*************************************************************************************/

function valida_cambio_clave()
{
	if(vacio(document.formulario_cambiar.actual.value) == false)
	{	alert("El campo 'CONTRASEÑA ACTUAL' no debe de estar vacio.");
      	document.formulario_cambiar.actual.focus();
      	return false;
	}
	if(vacio(document.formulario_cambiar.nueva.value) == false)
	{	alert("El campo 'NUEVA CONTRASEÑA' no debe de estar vacio.");
      	document.formulario_cambiar.nueva.focus();
      	return false;
	}
	if(vacio(document.formulario_cambiar.confirmacion.value) == false)
	{	alert("El campo 'CONFIRMACION' no debe de estar vacio.");
      	document.formulario_cambiar.confirmacion.focus();
      	return false;
	}
	if(valida_contrasena(document.formulario_cambiar.actual.value) == false)
	{	alert("Introdujo un caracter no valido en 'CONTRASEÑA ACTUAL'.");
      	document.formulario_cambiar.actual.focus();
      	return false;
	}
	if(valida_contrasena(document.formulario_cambiar.nueva.value) == false)
	{	alert("Introdujo un caracter no valido en 'NUEVA CONTRASEÑA'.");
      	document.formulario_cambiar.nueva.focus();
      	return false;
	}
	if(valida_contrasena(document.formulario_cambiar.confirmacion.value) == false)
	{	alert("Introdujo un caracter no valido en 'CONFIRMACION'.");
      	document.formulario_cambiar.confirmacion.focus();
      	return false;
	}
	if(validarLogitud(document.formulario_cambiar.actual.value.length,6,16)==false)
	{	alert("La 'CONTRASEÑA ACTUAL' debe tener de 6 a 16 caracteres.");
		document.formulario_cambiar.actual.focus();
		return false;
	}
	if(validarLogitud(document.formulario_cambiar.nueva.value.length,6,16)==false)
	{	alert("La 'NUEVA CONTRASEÑA' debe tener de 6 a 16 caracteres.");
		document.formulario_cambiar.nueva.focus();
		return false;
	}
	if((document.formulario_cambiar.nueva.value)!=(document.formulario_cambiar.confirmacion.value))
	{	alert("La contraseña nueva y su confirmación deben ser iguales.");
		document.formulario_cambiar.nueva.value = '';
		document.formulario_cambiar.confirmacion.value = '';
		return false;
	}

//alert("pasa");
//return false;

	var id = caracter_malo(document.formulario_cambiar.id.value);
	var pass = caracter_malo(document.formulario_cambiar.pass.value);
	var actual = caracter_malo(document.formulario_cambiar.actual.value);
	var nueva = caracter_malo(document.formulario_cambiar.nueva.value);
	var tipo = caracter_malo(document.formulario_cambiar.tipo.value);

	var parametros="tipo="+tipo+"&id="+id+"&nueva="+nueva+"&pass="+pass+"&actual="+actual;
	//alert(parametros);
  	$.ajax({ async:true, type: "GET", dataType: "html", url:"modulos/administracion/usuario_sistema/ajax/cambiarClave.php", data:parametros, success:llegadacambiarClave, timeout:4000 });

}

function llegadacambiarClave(datos)
{		
 		if(datos==1){
		alert("La Contraseña fue cambiada correctamente.");
		//setTimeout("window.location='interfaz.php'");
		updateView('modulos/instrucciones/index.php');
 		return true;
 		}
 		if(datos==2){
 		alert("La Contraseña Actual es invalida.");
 		return false;
 		}
 		if(datos==3){
 		alert("La Contraseña fue cambiada correctamente. por administrador");
		//setTimeout("window.location='interfaz.php'");
		updateView('modulos/instrucciones/index.php');
 		return false;
 		}

}


/*************************************************************************************
      FUNCION PARA VALIDAR EL MODULO DE CAMBIO DE CLAVE
*************************************************************************************/

function valida_cambio_clave_ad()
{

	if(vacio(document.formulario_cambiar_ad.nueva.value) == false)
	{	alert("El campo 'NUEVA CONTRASEÑA' no debe de estar vacio.");
      	document.formulario_cambiar.nueva.focus();
      	return false;
	}
	if(vacio(document.formulario_cambiar_ad.confirmacion.value) == false)
	{	alert("El campo 'CONFIRMACION' no debe de estar vacio.");
      	document.formulario_cambiar.confirmacion.focus();
      	return false;
	}

	if(valida_contrasena(document.formulario_cambiar_ad.nueva.value) == false)
	{	alert("Introdujo un caracter no valido en 'NUEVA CONTRASEÑA'.");
      	document.formulario_cambiar.nueva.focus();
      	return false;
	}

	if(valida_contrasena(document.formulario_cambiar_ad.confirmacion.value) == false)
	{	alert("Introdujo un caracter no valido en 'CONFIRMACION'.");
      	document.formulario_cambiar.confirmacion.focus();
      	return false;
	}
	if(validarLogitud(document.formulario_cambiar_ad.nueva.value.length,6,16)==false)
	{	alert("La 'NUEVA CONTRASEÑA' debe tener de 6 a 16 caracteres.");
		document.formulario_cambiar.nueva.focus();
		return false;
	}
	if((document.formulario_cambiar_ad.nueva.value)!=(document.formulario_cambiar_ad.confirmacion.value))
	{	alert("La contraseña nueva y su confirmación deben ser iguales.");
		document.formulario_cambiar_ad.nueva.value = '';
		document.formulario_cambiar_ad.confirmacion.value = '';
		return false;
	}


	var id = caracter_malo(document.formulario_cambiar_ad.id.value);
	var nueva = caracter_malo(document.formulario_cambiar_ad.nueva.value);
	var tipo = caracter_malo(document.formulario_cambiar_ad.tipo.value);
	var parametros="tipo="+tipo+"&id="+id+"&nueva="+nueva;
	//alert(parametros);
  	$.ajax({ async:true, type: "GET", dataType: "html", url:"modulos/administracion/usuario_sistema/ajax/cambiarClave.php", data:parametros, success:llegadacambiarClave, timeout:4000 });



}



/***********************************************************************************
							FUNCION PARA LA VALIDACION DEL FORMULARIO								
***********************************************************************************/

function valida_usuario()
{
	if(vacio(document.formulario.cedula.value) == false)
	{	alert("El campo 'CÉDULA' no debe de estar vacio.");
      	document.formulario.cedula.focus();
      	return false;
	}

	if(validarLogitud(document.formulario.cedula.value.length,4,8)==false)
		{	alert("La CÉDULA debe tener de 4 a 8 caracteres.");
			document.formulario.cedula.focus();
			return false;
		}

	if(vacio(document.formulario.nombre_apellido.value) == false)
	{	alert("El campo 'NOMBRE y APELLIDO' no debe de estar vacio.");
      	document.formulario.nombre_1.focus();
      	return false;
	}

	if(busca_caracter(document.formulario.nombre_apellido.value) == false)
	{	alert("El campo 'NOMBRE y APELLIDO' solo debe contener letras o numeros.");
      	document.formulario.nombre_1.focus();
      	return false;
	}

	if(vacio(document.formulario.cargo.value) == false)
	{	alert("El campo 'CARGO' no debe de estar vacio.");
      	document.formulario.cargo.focus();
      	return false;
	}

	if(busca_caracter(document.formulario.cargo.value) == false)
	{	alert("El campo 'CARGO' solo debe contener letras o numeros.");
      	document.formulario.cargo.focus();
      	return false;
	}

    if(vacio(document.formulario.login.value) == false)
	{	alert("El campo 'USUARIO' no debe de estar vacio.");
      	document.formulario.login.focus();
      	return false;
	}

    if(busca_caracter(document.formulario.login.value) == false)
	{	alert("El campo 'USUARIO' solo debe contener letras o numeros.");
      	document.formulario.login.focus();
      	return false;
	}

    if(vacio(document.formulario.contrasena.value) == false)
	{	alert("El campo 'CONTRASEÑA' no debe de estar vacio.");
      	document.formulario.contrasena.focus();
      	return false;
	}

		if((validarLogitud(document.formulario.contrasena.value.length,6,16)==false)&&(document.formulario.accion.value==1))
		{	alert("La CONTRASEÑA debe tener de 6 a 16 caracteres.");
			document.formulario.contrasena.focus();
			return false;
		}
	
		if((vacio(document.formulario.confirmacion.value) == false)&&(document.formulario.accion.value==1))
		{	alert("Debe CONFIRMAR la contraseña.");
			document.formulario.confirmacion.focus();
			return false;
		}
	
		if(((document.formulario.contrasena.value)!=(document.formulario.confirmacion.value))&&(document.formulario.accion.value==1))
		{	alert("La contraseña y su confirmación deben ser iguales.");
			document.formulario.contrasena.value = '';
			document.formulario.confirmacion.value = '';
			return false;
		}

	
		if(vacio(document.formulario.correo_electronico.value) == true)
		{	
		if(validarEmail(document.formulario.correo_electronico.value) == false)
		{	alert("La dirección de CORREO ELECTRONICO es incorrecta.");
			document.formulario.correo_electronico.focus();
			return false;
		}
		}

    if(vacio(document.formulario.correo_electronico.value) == false)
	{	
	correo_electronico = 'NO POSEE';
	}

    if(vacio(document.formulario.correo_electronico.value) == true)
	{	
	correo_electronico = caracter_malo(document.formulario.correo_electronico.value);
	}
	
	var login = (document.formulario.login.value);
	var cedula = (document.formulario.cedula.value);
	if(document.formulario.accion.value==1){
	var id_usuario=0;
	}else{
	var id_usuario= document.formulario.id_usuario.value;
	}
	var para="login="+login+"&id_usuario="+id_usuario+"&cedula="+cedula;
	
 	$.ajax({ async:true, type: "GET", dataType: "html",contentType: "application/x-www-form-urlencoded", url:"modulos/administracion/usuario_sistema/ajax/validarExistencia.php", data:para, success:llegadaValidacionINS, timeout:6000 }); 
}

function llegadaValidacionINS(datos)
{		
		if(datos==1){
		alert("La CEDULA ingresada ya existe");
		return false;
		}
		if(datos==2){
		alert("El USUARIO ingresado ya existe");
		return false;
		}
		if (datos==0){

		var nombre_apellido = caracter_malo(document.formulario.nombre_apellido.value);
		var login = caracter_malo(document.formulario.login.value);
		var contrasena = (document.formulario.contrasena.value);
		var id_perfil = caracter_malo(document.formulario.id_perfil.value);
		var id_dependencia = caracter_malo(document.formulario.id_dependencia.value);	
		var accion= caracter_malo(document.formulario.accion.value);
		var cargo = caracter_malo(document.formulario.cargo.value);	
		var cedula= caracter_malo(document.formulario.cedula.value);
		var recibe= caracter_malo(document.formulario.recibe.value);
		if(accion==1){
		var id_usuario=0;
		}else{
		var id_usuario= document.formulario.id_usuario.value;
		}
		var parametros="nombre_apellido="+nombre_apellido+"&correo_electronico="+correo_electronico+"&login="+login+"&contrasena="+contrasena+"&id_perfil="+id_perfil+"&id_usuario="+id_usuario+"&accion="+accion+"&id_dependencia="+id_dependencia+"&cedula="+cedula+"&cargo="+cargo+"&recibe="+recibe;

 		ejecutarUsuario(parametros);
		return false;
		}
}





/*******************************************************************
DESCRIPCION: Funcion para ELIMINAR CARACTERES NO VALIDO
********************************************************************/
	function caracter_malo(checkStr)
	{
		var checkBAD = "*/-'#\"\\";
		var checkFinal="";
		for (i = 0;  i < checkStr.length;  i++)
		{
			band=0;
			c1 = checkStr.charAt(i);
			for (j = 0;  j < checkBAD.length;  j++)
			{
				c2 = checkBAD.charAt(j);
				if(c2==c1 )
				{
					band=1; break;
				}
			}
			if (band == 0)
			{
				checkFinal=checkFinal+c1;
			}
		}
		return checkFinal;  
  }

/************************************************************************
DESCRIPCION: busca caracteres que no sean espacio en blanco en una cadena
*************************************************************************/
function vacio(campo) {
	for ( i = 0; i < campo.length; i++ ) {
		if ( campo.charAt(i) != "0" ) {
			return true
		}
	}
	return false
}

/************************************************************************
DESCRIPCION: busca caracteres que no sean numeros
*************************************************************************/
function busca_nonum(checkStr) {
var checkOK ="0123456789";
var allValid = true;
for (i = 0; i < checkStr.length; i++) {
ch = checkStr.charAt(i);
for (j = 0; j < checkOK.length; j++)
if (ch == checkOK.charAt(j))
break;
if (j == checkOK.length) {
allValid = false;
break;
}
}
if (!allValid) {
return (false);
}
}

/************************************************************************
DESCRIPCION: busca caracteres que no sean numeros o letras
*************************************************************************/
function busca_caracter(checkStr) {
var checkOK = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚ" + "abcdefghijklmnñopqrstuvwxyzáéíóú " + "0123456789";
var allValid = true;
for (i = 0; i < checkStr.length; i++) {
ch = checkStr.charAt(i);
for (j = 0; j < checkOK.length; j++)
if (ch == checkOK.charAt(j))
break;
if (j == checkOK.length) {
allValid = false;
break;
}
}
if (!allValid) {
return (false);
}
}

/************************************************************************
DESCRIPCION: valida que las contraseñas tengas solo letras numeros y punto
*************************************************************************/
function valida_contrasena(checkStr) {
var checkOK = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ" + "abcdefghijklmnñopqrstuvwxyz." + "0123456789";
var allValid = true;
for (i = 0; i < checkStr.length; i++) {
ch = checkStr.charAt(i);
for (j = 0; j < checkOK.length; j++)
if (ch == checkOK.charAt(j))
break;
if (j == checkOK.length) {
allValid = false;
break;
}
}
if (!allValid) {
return (false);
}
}

/************************************************************************
DESCRIPCION: valida el Email ingresado
*************************************************************************/

function validarEmail(valor) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor)){
	return (true);
	} else {
	return (false);
	}
}

/************************************************************************
DESCRIPCION: valida la longitud de los datos ingresados
*************************************************************************/

function validarLogitud(valor,min,max) {
	if ((valor >= min)&&(valor <= max)){
	return (true);
	} else {
	return (false);
	}
}
/***********************************************************************************
							FUNCION AJAX PARA LA ELIMINACION DEL PERFIL								
***********************************************************************************/
function eliminarUsuariosis(parametros)
{	//alert(parametros);
  	$.ajax({ async:true, type: "GET", dataType: "html", url:"modulos/administracion/usuario_sistema/ajax/eliminar.php", data:parametros, beforeSend:inicioEliminar, success:llegadaEliminar, timeout:4000, error:problemasEliminar }); 
	return false;
}

function inicioEliminar()
{

      
}

function llegadaEliminar(datos)
{

	setTimeout("javascript:updateView('modulos/administracion/usuario_sistema/index.php')"); 
}

function problemasEliminar()
{

}

function eliminarusu(id){
	var confirmar=confirm("¿Realmente desea eliminar este Usuario?")
			if(confirmar == true){
				var id_usuario=id;
				var param="id_usuario="+id_usuario;
				eliminarUsuariosis(param);
			}
}
