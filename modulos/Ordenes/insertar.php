<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ordenes</title>
    <script type="text/javascript" src="/SAO/Librerias/Librerias.js"></script>

    <!-- Bootstrap -->
    
      <link rel="stylesheet" href="/SAO/css/bootstrap.min3.css">

      <link rel="stylesheet" href="/SAO/css/navbar-static-top.css">
      
      <script type="text/javascript" src="/SAO/js/script.js"></script>
      <script src="/SAO/js/jquery.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>
         <form id="vali"  action="javascript:registraOrden();" >
          <table class="table table-striped table-hover ">
             
            <tr class="active">
              <br/>
              <td>.::Insertar Instrucción::.</td>
              <td></td>
            </tr>
            </tbody>
            
            <tr>
              <td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; Instrucción</td>
              <td><div class="form-group">
  <input class="form-control input-sm" type="text" id="instruccion">
</div>

<div class="form-group"></td>
            </tr>
            <tr>
              <td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; Descripción</td>
             <td> 
              <div class="form-group">
  <input class="form-control input-lg" type="text" id="descrip">
</div>

<div class="form-group"></td>
            </tr>
            <tr>
              <td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; Responsable(s)</td>
              <td>

                <div class="controls">
                  <a   href="javascript:void(0)" onClick="javascript:mostraragenda();">Click Para Seleccionar los Responsables</a>
                  <br/>
                    <div id="dataagenda1" class= "oculta"> 
                      <div >
                        <select id="departa" name="departa" class="form-control input-md" onfocus="llenar();" onchange="cargarres();">
                          <option align="center" value="1">SELECCIONE DEPENDENCIA </option>
                        </select>
                      </div>

                        <div id="dataagenda"  > 
                        </div>

                  </div>
                </div>
            </td>
            </tr>

            <tr>
              <td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; Fecha de entrega</td> 

<td> <input type="date" id = "fecha1" name="fecha1" step="1" min="2013-01-01" max="2018-12-31" value="2015-01-01"></td>

              </tr>
            
          
          
        </div>

      </div>
      </table>

      <div class="container">
        <div class="row clearfix">
          <div class="col-md-3 column">
          </div>
          <div class="col-md-4 column">
            <a href="javascript:void(0)" class="btn btn-primary" onClick="javascript:updateView('modulos/Ordenes/index.php');">Volver&nbsp;&nbsp; </a>
            <input type="submit" value="Insertar" id="submit" class="btn btn-primary"/>
          </div>
          <div class="col-md-4 column">
          </div>
        </div>
      </div>
    </form>

 <br/>
<br/>
<br/>    

      <script src="/SAO/js/bootstrap.min2.js"></script>
      <script src="/SAO/js/main.js"></script>
    </body>
</html>