//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'sao'
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var depModel = {};

depModel.FindId = function(id,callback)
{
    if (connection) 
    {
        var sql = 'SELECT * FROM departamento WHERE id = ' + connection.escape(id);
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}

// consulta los usuarios de un departamento
depModel.FindUsers = function(id,callback)
{
    if (connection) 
    {
        var sql = 'SELECT u.id id, CONCAT(u.primer_nombre," ",u.primer_apellido) as nombre,c.nombre cargo FROM ocupacion o join cargo c on(c.id=o.CGO_id) join usuario u on (u.id= USO_id)' + 
        			'WHERE o.DPO_id = ' + connection.escape(id) + 'order by 2';

        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}

// retorna el departamento al que pertenece un usuario 
depModel.FindDep = function(id,callback)
{
    if (connection) 
    {
        var sql = 'select d.id id, d.nombre nombre from usuario u join ocupacion o on(u.id=o.USO_id) join departamento d on (d.id=o.DPO_id) where u.id =' + connection.escape(id);

        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}
// retorna los departamneto hijo 
depModel.FindDepHijo = function(id,callback)
{
    if (connection) 
    {
        var sql = 'select dh.id id, dh.nombre nombre from departamento dh where dh.DPO_id=' + connection.escape(id);


        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}



module.exports = depModel;