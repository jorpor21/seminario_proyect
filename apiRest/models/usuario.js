//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'sao'
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var usuarioModel = {};

//autenticacion de usuario
usuarioModel.Auth= function(nick,contrasena,callback)
{
    if (connection) 
    {
        var sql = 'SELECT  login, id FROM usuario WHERE (login = ' + connection.escape(nick) + ' or email = ' + connection.escape(nick) + ') and contraseña=' +connection.escape(contrasena) ;
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}

//retorna todos los usuarios
usuarioModel.FindAll = function(callback)
{
    if (connection) 
    {
        connection.query('SELECT id,cedula,primer_nombre,primer_apellido,telefono,email FROM usuario ORDER BY cedula', function(error, rows) {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, rows);
            }
        });
    }
}
//retorna un usuario segun su id
usuarioModel.FindId = function(id,callback)
{
    if (connection) 
    {
        var sql = 'SELECT * FROM usuario WHERE id = ' + connection.escape(id);
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}

usuarioModel.InsetarUsuario = function(usuarioData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO usuario SET ?', usuarioData, function(error, result) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                //devolvemos la última id insertada
                callback(null,{"insertId" : result.insertId});
            }
        });
    }
}

usuarioModel.modificar = function(userData, callback)
{
    //console.log(userData); return;
    if(connection)
    {
		var id =userData.id;
        var sql = 'UPDATE usuario SET cedula = ' + connection.escape(userData.cedula) + ',' +  
        'primer_nombre = ' + connection.escape(userData.primer_nombre) +',' + 
        'segundo_nombre = ' + connection.escape(userData.segundo_nombre) +',' + 
        'primer_apellido = ' + connection.escape(userData.primer_apellido) +',' + 
        'segundo_apellido = ' + connection.escape(userData.segundo_apellido) +',' + 
        'telefono = ' + connection.escape(userData.telefono) +',' + 
        'email = ' + connection.escape(userData.email) +',' + 
        'login = ' + connection.escape(userData.login) +',' + 
        'contraseña = ' + connection.escape(userData.contraseña) +',' + 
        'recibe_instruc = ' + connection.escape(userData.recibe_instruc) +
       
        ' WHERE id = ' + id;
 
        connection.query(sql, function(error, result) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null,{"msg":"se ha modificado el usuario con exito"});
            }
        });
    }
}

usuarioModel.permisos=function(Pid,Mid,callback)
{
    if (connection) 
    {
     
        var sql="SELECT insertar,modificar,consultar, eliminar FROM permisos WHERE ROL_id="+ connection.escape(Pid) + " AND MDO_id="+ connection.escape(Mid);
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}



module.exports = usuarioModel;