//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'sao'
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var ordenModel = {};

 ordenModel.FindOrdenes= function(id,oid,callback)
{
    if (connection) 
    {
        if(oid!='null'){
                var sql = 'SELECT i.id id, i.titulo titulo,i.descripcion, i.fecha_entrega entrega, r.creador creador FROM responsable r '+
                    'join instruccion i on(r.ITO_id=i.id) join usuario u on (u.id= r.USO_id)' + 
                    'WHERE r.USO_id = ' + connection.escape(id);
            }
            else{
               
                var sql = 'SELECT i.id id, i.titulo titulo,i.descripcion, i.fecha_entrega entrega, r.creador creador FROM responsable r '+
                    'join instruccion i on(r.ITO_id=i.id) join usuario u on (u.id= r.USO_id)' + 
                    'WHERE r.USO_id = ' + connection.escape(id);
            }

        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}
ordenModel.FindDetallesOrdenes= function(id,callback)
{
    if (connection) 
    {
        
                var sql = 'SELECT distinct  i.id id, i.titulo titulo,i.descripcion, i.fecha_entrega entrega, i.fecha_creacion creacion FROM responsable r '+
                    'join instruccion i on(r.ITO_id=i.id) join usuario u on (u.id= r.USO_id)' + 
                    'WHERE i.id='+ connection.escape(id);
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }
}

ordenModel.FindResponsables= function(id,callback){
 if (connection) 
    {
        var sql = 'SELECT u.id id, concat(u.primer_nombre," ",primer_apellido) nombre, u.email correo, c.nombre cargo, r.creador creador from responsable r join usuario u on (r.USO_id = u.id)'+
                                        'join ocupacion oc on(oc.USO_id= u.id) join cargo c on (c.id= oc.CGO_id) where r.ITO_id ='+  connection.escape(id);

          connection.query(sql, function(error, row) 
          {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });
    }

}


ordenModel.InsetarOrden = function(ordenData,resp,callback)
{
    if (connection) 
    {
        var resDAta={};
        connection.query('INSERT INTO instruccion SET ?', ordenData, function(error, result) 
        {
            if(error)
            {
                throw error;
            }
            else
            {



                 resp.forEach(function(valorDelElemento, indiceDelElemento)
                 {
                         resDAta[indiceDelElemento]={USO_id: valorDelElemento,ITO_id:result.insertId,creador: 0};

                                   connection.query('INSERT INTO responsable SET ?', resDAta[indiceDelElemento], function(error2, resulta) 
                                {
                                        if(error2){

                                            throw error2;    

                                        }
                              
                                
                                
                             });
                 }); 

                  
                           
                   
                    

                callback(null, {msg:"Sucess"});
                 
            }
        });
    }
}

module.exports = ordenModel;