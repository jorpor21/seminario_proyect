var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http=require('http')
var routes = require('./routes/index');
var users = require('./routes/users');
var usuario = require('./routes/usuario');
var dep = require('./routes/dep');
var orden = require('./routes/orden');
var session = require('express-session');


var app = express();
function perimitirCrossDomain(req, res, next) {
  //en vez de * se puede definir SÓLO los orígenes que permitimos
  res.header('Access-Control-Allow-Origin', '*');
  //metodos http permitidos para CORS
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
}


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(perimitirCrossDomain);
app.set("jsonp callback", true);
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({name: 'apisao',secret: 'ssshhhhh',  resave: false,  saveUninitialized: true}));
app.use('/', routes);
app.use('/users', users);
app.use('/usuario', usuario);
app.use('/dep', dep);
app.use('/orden', orden);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
	
    err.status = 404;
    next(err);
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
	app.get('/prueba',function(req,res){
		res.send('Accion de prueba');
	});
	
});


module.exports = app;
