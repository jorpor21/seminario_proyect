var express = require('express');
var router = express.Router();
var ordenModel = require('../models/orden');


// retorna las instrucciones o detalles de una instruccion de un usuario 
router.get("/api/ord/:id", function(req,res)
    {
        //id de usuario e instruccion
        var id = req.params.id;
        var Oid = req.params.oid;
        //solo actualizamos si la id es un número
        if(!isNaN(id) )
        {
            ordenModel.FindOrdenes(id,Oid,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.jsonp(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.jsonp(404,{msg:"Sin Instrucciones"});
                }
            });
        }
        //si hay algún error
        else
        {
            res.jsonp(500,{msg:"Parametros Invalidos"});
        }
});

router.get("/api/ordet", function(req,res)
    {
        //id de usuario e instruccion
       
        var id = req.param('id');
       
        
        //solo actualizamos si la id es un número
        if(!isNaN(id) )
        {
            ordenModel.FindDetallesOrdenes(id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.jsonp(200,data[0]);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.jsonp(404,{msg:"No se Encontro la instruccion"});
                }
            });
        }
        //si hay algún error
        else
        {
            res.jsonp(500,{msg:"Parametros Invalidos"});
        }
});

//retorna los responsables de una instruccion
router.get("/api/ordres/:id", function(req,res)
    {
        //id de la Instruccion
        var id = req.params.id;
        //solo actualizamos si la id es un número
        if(!isNaN(id) )
        {
            ordenModel.FindResponsables(id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.jsonp(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.jsonp(404,{msg:"Sin Responsables"});
                }
            });
        }
        //si hay algún error
        else
        {
            res.jsonp(500,{msg:"Parametros Invalidos"});
        }
});


// insertar una nueva instruccion
router.post("/api/ord/nueva", function(req,res)
    {
        //creamos un objeto con los datos a insertar del usuario
        var ordenData = {
            id : null,
            titulo : req.body.titulo,
            descripcion : req.body.descripcion,
            fecha_creacion : null,
            fecha_entrega : req.body.fecha_entrega            
            
        };
        var resp= req.body.responsables.split("/");


            

        ordenModel.InsetarOrden(ordenData,resp,function(error, data)
        {
            //si el usuario se ha insertado correctamente mostramos su info
            if(data.msg==="Sucess" )
            {
               res.jsonp(200,{msg:"Sucess"});
            }
            else
            {
                res.jsonp(500,{msg:"Error"});
            }
        });
    });

module.exports = router;