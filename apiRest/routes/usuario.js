var express = require('express');
var router = express.Router();
var usuarioModel = require('../models/usuario');
var sess;
/* GET users listing. */
router.get("/api/sess", function(req,res)
    
        {
         req.session.user='portela';
            
            res.jsonp(req.session.user);
        }
        
    );
router.get("/api/session", function(req,res)
    
        {
         
            
            res.jsonp({"nombre":req.session.user,"userid":req.session.userid});
        }
        
    );
//retorna todos los usuarios
router.get("/api/usuarios", function(req,res){
	//if(req.session.user){
        usuarioModel.FindAll(function(error, data)
        {
		 if (typeof data !== 'undefined' && data.length > 0)
			{
                    res.jsonp(data);
			}
			else
			{
				res.jsonp(500,{"msg":"Error"});
			}
            
        });
	//}else
	//	res.jsonp(500,{msg:"Requiere Autenticación"});
});
	
//retorna un usuario segun su id	
router.get("/api/usuario/:id", function(req,res)
    {
        //id del usuario
        var id = req.params.id;
        //solo actualizamos si la id es un número
        if(!isNaN(id))
        {
            usuarioModel.FindId(id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato jsonp
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.jsonp(data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.jsonp(404,{"msg":"El Usuario no Existe"});
                }
            });
        }
        //si hay algún error
        else
        {
            res.jsonp(500,{"msg":"Error"});
        }
});	

//Retorna los permisos de en perfil en un modulo
 router.get("/api/perfil/:pid/:mid", function(req,res)
    {
        //id del perfil
        var Pid = req.params.pid;

        //id del modulo
        var Mid = req.params.mid;

        //solo actualizamos si la id es un número
        if(!isNaN(Pid) && !isNaN(Mid))
        {
            usuarioModel.permisos(Pid,Mid,function(error, data)
            {
                //si el usuario existe lo mostramos en formato jsonp
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.jsonp(data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.jsonp(404,{msg:error});
                }
            });
        }
        //si hay algún error
        else
        {
            res.jsonp(500,{msg:"Parametros Invalidos"});
        }
    }); 
	
//Redirecciona al formulario
router.get("/insertar", function(req, res){
        res.render("nuevo",{ 
            title : "Formulario para Registrar un nuevo Usuario"
        });
    });
	router.get("/api/update/:id", function(req, res){
        var id = req.params.id;
        //solo actualizamos si la id es un número
        if(!isNaN(id))
        {
            usuarioModel.FindId(id,function(error, data)
            {
                //si existe el usuario mostramos el formulario
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.render("modificar",{ 
                        title : "Formulario Para Modificar Usuario", 
                        info : data
                    });
                }
                //en otro caso mostramos un error
                else
                {
                    res.jsonp(404,{msg:"notExist"});
                }
            });
        }
        //si la id no es numerica mostramos un error de servidor
        else
        {
            res.jsonp(500,{"msg":"The id must be numeric"});
        }
    });
	
	//post 
//autenticacion de usuario	
router.get("/api/auth", function(req,res)
    {
      
        var nick = req.param('usuario');
        var password = req.param('password');
			
        usuarioModel.Auth(nick,password,function(error, data)
        {

            if(typeof data !== 'undefined' && data.length > 0)
            {
				
                req.session.user=data[0].login;           
				req.session.userid=data[0].id;           
                res.jsonp({msg:"Sucess"});
            }
            else
            {
                res.jsonp(400,{msg:"Credenciales Invalidas"});
            }
        });
    });
	
router.post("/api/create", function(req,res)
    {
        //creamos un objeto con los datos a insertar del usuario
        var usuarioData = {
            id : null,
            cedula : req.body.cedula,
			primer_nombre : req.body.primer_nombre,
			segundo_nombre : null,
			primer_apellido : req.body.primer_apellido,
			segundo_apellido :null,
            telefono : req.body.telefono,
            email : req.body.email,
            login : req.body.login,
            contraseña : req.body.contraseña,
			recibe_instruc:0,
            ROL_id : req.body.perfil
			
            
        };
        usuarioModel.InsetarUsuario(usuarioData,function(error, data)
        {
            //si el usuario se ha insertado correctamente mostramos su info
            if(data && data.insertId)
            {
                
                res.send("El usuario se ha insertado correctamente");
            }
            else
            {
                res.jsonp(500,{"msg":"Error"});
            }
        });
    });
	
	//put
	
router.post("/api/modificar", function(req,res)
    {
        //almacenamos los datos del formulario en un objeto
       
		var usuarioData = {
            id : req.param('id'),
            cedula : req.param('cedula'),
			primer_nombre : req.param('primer_nombre'),
			segundo_nombre : req.param('segundo_nombre'),
			primer_apellido : req.param('primer_apellido'),
			segundo_apellido :req.param('segundo_apellido'),
            telefono : req.param('telefono'),
            email :req.param('email'),
            login :req.param('login'),
            contraseña : req.param('contraseña'),
			recibe_instruc:req.param('recibe_instruc'),
            ROL_id : req.param('ROL_id')
			
            
        };
		
        usuarioModel.modificar(usuarioData,function(error, data)
        {
            //si el usuario se ha actualizado correctamente mostramos un mensaje
            if(data && data.msg)
            {
                res.jsonp(data);
            }
            else
            {
                res.jsonp(500,{"msg":"Error"});
            }
        });
    });


//cerrar la session
router.get("/api/logout",function(req,res){

     req.session.destroy();
     
     res.jsonp({msg:"sucess"});
     
});
	
	
	
module.exports = router;