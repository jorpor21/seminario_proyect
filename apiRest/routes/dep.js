var express = require('express');
var router = express.Router();
var depModel = require('../models/dep');


//Retornar detalles de un departamento dado su id
router.get("/api/dep/:id", function(req,res)
    {
        //id del departamento
        var id = req.params.id;
        //solo actualizamos si la id es un número
        if(!isNaN(id))
        {
            depModel.FindId(id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.json(404,{"msg":"El Departamento no Existe"});
                }
            });
        }
        //si hay algún error
        else
        {
            res.json(500,{"msg":"Error"});
        }
});
// retorna todos los usuarios de departamento
router.get("/api/usu/:id", function(req,res)
    {
        //id del departamento
        var id = req.params.id;
        //solo actualizamos si la id es un número
        if(!isNaN(id))
        {
            depModel.FindUsers(id,function(error, data)
            {
                //si los usuarios existen los mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.json(404,{msg:"Sin Registros"});
                }
            });
        }
        //si hay algún error
        else
        {
            res.json(500,{msg:"Parametros Invalidos"});
        }
});


router.get("/api/ldep/:id", function(req,res)
    {
        //id del departamento
        var id = req.params.id;
        //solo actualizamos si la id es un número
        if(!isNaN(id))
        {
            depModel.FindDep(id,function(error, data)
            {
                //si los usuarios existen los mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    depModel.FindDepHijo(data[0].id,function(error, data2)
                    {
                        //si los usuarios existen los mostramos en formato json
                        if (typeof data2 !== 'undefined' && data2.length > 0)
                        {
                            var lista=data;

                            data2.forEach(function(valor, indice)
                             {
                                lista[indice+1]=data2[indice];
                             });

                             res.json(200,lista);
                        }
                        //en otro caso mostramos una respuesta conforme no existe
                        else
                        {
                            res.json(404,{msg:"Sin Registros1"});
                        }
                    });
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.json(404,{msg:"Sin Registros2"});
                }
            });
        }
        //si hay algún error
        else
        {
            res.json(500,{msg:"Parametros Invalidos"});
        }
    });



module.exports = router;