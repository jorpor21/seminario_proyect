var express = require('express');
var router = express.Router();
var UserModel = require('../models/users');
/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Expressos occidente' });
});

router.get("/usuarios", function(req,res){
        UserModel.getUsers(function(error, data)
        {
            res.json(200,data);
        });
    });

router.get('/JORMAN/UNO', function(req, res) {
  res.send('UNO UNO UNO');
});

 router.get("/user/update/:id", function(req, res){
        var id = req.params.id;
        //solo actualizamos si la id es un número
        if(!isNaN(id))
        {
            UserModel.getUser(id,function(error, data)
            {
                //si existe el usuario mostramos el formulario
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.render("index",{ 
                        title : "Formulario", 
                        info : data
                    });
                }
                //en otro caso mostramos un error
                else
                {
                    res.json(404,{"msg":"notExist"});
                }
            });
        }
        //si la id no es numerica mostramos un error de servidor
        else
        {
            res.json(500,{"msg":"The id must be numeric"});
        }
    });

module.exports = router;
