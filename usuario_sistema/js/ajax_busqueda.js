
var divid_busca = "busca_usuario"; // el div que quieres!
var url_usuario_sistema = "modulos/administracion/usuario_sistema/busqueda.php"; // el archivo que ira en el div

////////////////////////////////
//
// Refreshing the DIV
//
////////////////////////////////

function refreshdiv_usuario(){

var u=document.getElementById("buscar").value;

if (u==''){
	location.href="javascript:updateView('modulos/administracion/usuario_sistema/index.php')";
	return false;
}

var xmlHttp;

try{
	xmlHttp=new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
}
catch (e){
	try{
		xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
	}
	catch (e){
		try{
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	catch (e){
			alert("Tu explorador no soporta AJAX.");
			return false;
		}
	}
}

var vari=1;
var nocacheurl = url_usuario_sistema+"?escrito="+u+"&vari="+vari;

xmlHttp.onreadystatechange=function(){
if(xmlHttp.readyState==4){
document.getElementById("busca_usuario").innerHTML=xmlHttp.responseText;
document.getElementById("buscar").focus();

}
}
xmlHttp.open("GET",nocacheurl,true);
xmlHttp.send(null);
}

// Cargador Principal

function updateAjax3(urlHref, form) {

    var loader  = '<div id="loader" >'+
                     '<img src="images/ajax-loader.gif">'+
                  '</div>';


    form        = typeof(form) != 'undefined' ? form : '';

    var tipo    = 'get';
    var datos   = '';
    var dataVal = '';


    if (form != '' ) {

        $('#'+form+' :input').each(

            function() {


                value = '';

                //if ( $.browser.msie )
                    value   = this.value;

                //else
                //    value   = this.type == 'textarea' ? escape(this.value) : this.value;


                dataVal = this.name+'='+value+'&'
                datos   = datos + dataVal;

            }
        );

        tipo = form == 'buscador' ? 'get' : 'post';

    } else {

        var str = urlHref.split('?');
        urlHref = str[0];
        datos   = str[1];

    }

    $.ajax({
        type       : tipo,
        url        : urlHref,
        data       : datos,
        dataType   : 'html',
        cache      : false,
        beforeSend : function () {
            $("#top").append(loader);
        },
        success    : function (html) {

            if ($("#loader").fadeOut('slow'))
                $("#loader").remove();

            $("#busca_usuario").html(html);

        }
    });
}




